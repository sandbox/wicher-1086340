Limitations:
- doesn't work for anonymous users
- only tested on MySQL

Work you have to do yourself:
- insert a competition into the database
- insert some pools into the database

Wishlist:
- integrate with Views, or provide block
- parameterize with admin menu, enabling other matches than best-of-five (maybe with match types - plugins of some sort?)
- forms to create new competitions and pools
- themeing functions
- some DB optimization (extra indexes)

Blockers before a stable release:
- themeing functions
- forms to create new competitions and pools