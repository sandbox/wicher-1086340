<?php

/**
 * @file
 * Allows pooled competitions (best-of-five). Users can enter match results.
 *
 * @copyright Copyright (c) 2008-2011 Wicher Minnaard.
 *
 * Licensed under the terms of the GNU General Public License.
 * See http://drupal.org/licensing/faq#q1
 */

define('POOLMATCH_PATH', drupal_get_path('module', 'poolmatch'));
include(POOLMATCH_PATH .'/theming.inc');

// Some globals
$contender = '';
$opponent = '';
$poolid = '';
$matchid = '';
$contenderscore = '';
$opponentscore = '';


/**
 *Implementation of hook_cron().
 */
function poolmatch_cron() {
  $matchresult_nagging_timestamp = variable_get('poolmatch_matchresult_nagging_timestamp', '');
  $matchresult_nagging_interval = 3600 * variable_get('poolmatch_matchresult_nagging_interval', '');

if ($matchresult_nagging_interval)
  {
  if ((time() - $matchresult_nagging_timestamp) >= $matchresult_nagging_interval)
    {
    // Send email for every neglected matchresult
    $sql = 'SELECT DISTINCT u.name, u.mail FROM {users} u, {poolmatch_matches} pm, {poolmatch_match_results} pmr 
	     WHERE pm.mid = pmr.mid AND pm.creator <> pmr.uid AND u.uid = pmr.uid 
	     AND pm.ok = 0 AND pm.created < (NOW() - %d)';
    $result = db_query($sql, $matchresult_nagging_interval);
    while ($data = db_fetch_object($result))
      {
      $ismailsent = drupal_mail('poolmatchmail_matchresult_nagging', $data->mail, t('Herinnering: wedstrijduitslag goed- of afkeuren'), t('Er is nog immer een wedstrijduitslag van de BEAT IT-squashladder waarvoor jouw goed- of afkeuring nodig is. Of misschien zijn het er wel meerdere. We verzoeken je (met klem) hier actie op te ondernemen. Log in op http://www.usbeatit.nl en regel het.


      Liefs, de website.'));

      // Log if mail was actually sent
      if ($ismailsent) watchdog('poolmatch', 'matchresult-nagmail sent to '.$data->name.' at '.$data->mail,'WATCHDOG_NOTICE');
      }  
    variable_set('poolmatch_matchresult_nagging_timestamp', time());
    }
  }
}

/**
 * Implementation of hook_perm()
 */
function poolmatch_perm()
  {
  return array('access poolmatches', 'administer poolmatches');
  }

/**
 * Implementation of hook_menu()
 */

function poolmatch_menu($may_cache)
  {
  $items = array();
  if (!$may_cache)
    {
    $items[] = array(
      'path' => 'poolmatch/competition/view',
      'description' => t('Bekijk een competitie'),
      'callback' => 'viewCompetition',
      'access' => user_access('access poolmatches'),
      'type' => MENU_SUGGESTED_ITEM
    );
    $items[] = array(
      'path' => 'poolmatch/pool/view',
      'title' => 'Pooloverzicht',
      'description' => t('Bekijk een pool'),
      'callback' => 'viewPool',
      'access' => user_access('access poolmatches'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'poolmatch/matchresult/add',
      'title' => 'Matchuitslag',
      'description' => t('Voeg een uitslag toe of keur er ��n goed'),
      'callback' => 'addMatchResult',
      'access' => user_access('access poolmatches'),
      'type' => MENU_CALLBACK
    );
    $items[] = array(
      'path' => 'admin/settings/poolmatch',
      'title' => t('Poolmatch-instellingen'),
      'description' => t('Beheer het gedrag van de poolmatchmodule'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('poolmatch_admin_settings'),
      'access' => user_access('administer poolmatches')
    );
    }
  return $items;
  }


function addMatchResult($contenderparam, $opponentparam, $poolidparam)
  {
  // let's set our globals
  global $contender, $opponent, $poolid, $user;
  $contender = $contenderparam;
  $opponent = $opponentparam;
  $poolid = $poolidparam;

  // sanity checks:
  // a) $contender or $opponent is current user, or current user is admin, else -> fail
  // b) both $contender and $opponent are in pool $pid, and pool is in active competition, else -> fail

  // a)
//  $useruid = $user->uid;
  if (!(($user->uid == $contender) || ($user->uid == $opponent) || user_access('administer poolmatches'))) return t('<h2>I\'m sorry Dave...</h2><p>...but I\'m afraid I can\'t do that. Je probeert een uitslag van een wedstrijd die je zelf niet hebt gespeeld te bewerken.</p>');
  
  // b)
  $sql = 'SELECT mu1.uid 
	  FROM {poolmatch_users} mu1 INNER JOIN {poolmatch_users} mu2 ON mu1.pid = mu2.pid 
	  WHERE mu1.uid <> mu2.uid 
	  AND mu1.pid IN (SELECT poolmatch_pools.pid FROM {poolmatch_pools}, {poolmatch_competitions} WHERE poolmatch_pools.cid = poolmatch_competitions.cid AND poolmatch_competitions.active = 1) 
	  AND mu1.uid = %d AND mu2.uid = %d 
	  AND mu1.pid = %d';

  $resultobject = db_fetch_object(db_query($sql, $contender, $opponent, $poolid));
  if (!isset($resultobject)) return t('<h2>I\'m sorry Dave...</h2><p>...but I\'m afraid I can\'t do that. Je probeert een uitslag van een wedstrijd te maken die niet bestaat in de competitie, of de competitie waar de wedstrijd deel van uitmaakt is al afgelopen.</p>');

  // cases:
  // a) matchresults already exist and are OK -> fail
  // b) matchresults are being confirmed by the same user who entered them -> fail
  // c) no matchresults yet -> form
  // d) matchresult not OK yet -> form

  $sql = 'SELECT pmr1.uid as contender, pmr1.score as contenderscore, pmr2.uid as opponent, pmr2.score as opponentscore, pm.ok, pm.creator, pm.mid as matchid 
	  FROM {poolmatch_match_results} pmr1 INNER JOIN {poolmatch_match_results} pmr2 ON pmr1.mid = pmr2.mid, {poolmatch_matches} as pm
	  WHERE pmr1.uid <> pmr2.uid AND pmr1.mid = pm.mid 
	  AND pmr1.uid = %d AND pmr2.uid = %d AND pid = %d';

  $matchobject = db_fetch_object(db_query($sql, $contender, $opponent, $poolid));

  //c)
  if (!isset($matchobject)) return drupal_get_form('poolmatch_matchform_new');

  //a)
  if (($matchobject->ok == 1) && (!(user_access('administer poolmatches')))) return t('<h2>I\'m sorry Dave...</h2><p>...but I\'m afraid I can\'t do that. De uitslag van deze wedstrijd is vastgelegd. Als het goed is heb je deze zelf zo ingevuld of goedgekeurd.</p>');

  //b)
  if (($matchobject->creator == $user->uid) && (!(user_access('administer poolmatches')))) return t('<h2>I\'m sorry Dave...</h2><p>...but I\'m afraid I can\'t do that. Je kunt niet een uitslag bevestigen die je zelf hebt ingevoerd.</p>');

  //d)
  if (($matchobject->ok == 0) || (user_access('administer poolmatches')))
    {
    global $contenderscore, $opponentscore, $matchid;
    $contenderscore = $matchobject->contenderscore;
    $opponentscore = $matchobject->opponentscore;
    $matchid = $matchobject->matchid;
    return drupal_get_form('poolmatch_matchform_confirm');
    }
  return 'Danger, Will Robinson!';
  watchdog('poolmatch', 'unhandled AddMatchResult condition','WATCHDOG_ERROR');
}


function poolmatch_matchform_new()
  {
  global $contender, $contenderscore, $opponent, $opponentscore, $poolid;
  $usernamecontender = user_load(array('uid'=>$contender))->name;
  $usernameopponent  = user_load(array('uid'=>$opponent))->name;

  $form['contenderscore'] = array(
    '#field_suffix' => $usernamecontender,
    '#type' => 'textfield',
    '#maxlength' => '1',
    '#size' => '1',
    '#description' => t('Vul hier het aantal games in dat gewonnen is door '.$usernamecontender.'.'),
    );
  $form['opponentscore'] = array(
    '#field_suffix' => $usernameopponent,
    '#type' => 'textfield',
    '#maxlength' => '1',
    '#size' => '1',
    '#description' => t('Vul hier het aantal games in dat gewonnen is door '.$usernameopponent.'.'),
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    );
  return $form;
  }


function poolmatch_matchform_confirm()
  {
  global $contender, $contenderscore, $opponent, $opponentscore, $poolid;
  $usernamecontender = user_load(array('uid'=>$contender))->name;
  $usernameopponent  = user_load(array('uid'=>$opponent))->name;

  $form['contenderscore'] = array(
    '#field_suffix' => $usernamecontender,
    '#type' => 'textfield',
    '#value' => $contenderscore,
    '#maxlength' => '1',
    '#size' => '1',
    '#disabled' => 'disabled',
    );
  $form['opponentscore'] = array(
    '#field_suffix' => $usernameopponent,
    '#type' => 'textfield',
    '#value' => $opponentscore,
    '#maxlength' => '1',
    '#size' => '1',
    '#disabled' => 'disabled',
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Dat klopt ja'),
    );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Klopt niet'),
    );
  return $form;
  }


function poolmatch_matchform_confirm_submit($form, $form_values)
  {
  global $matchid, $poolid;
  if ($form_values['op'] == t('Dat klopt ja')) db_query('UPDATE {poolmatch_matches} SET ok = 1 WHERE mid = %d', $matchid);
  elseif  ($form_values['op'] == t('Klopt niet'))
    {
    db_query('DELETE FROM {poolmatch_match_results} WHERE mid = %d', $matchid);
    db_query('DELETE FROM {poolmatch_matches} WHERE mid = %d', $matchid);
    }
  return 'poolmatch/pool/view/'.$poolid;
  }


function poolmatch_matchform_new_validate($form_id, $form_values)
  {
  if ($form_values['contenderscore'] > 3) form_set_error('contenderscore',t('Bij een best-of-five win je nooit meer dan 3 games.'));
  if ($form_values['opponentscore'] > 3) form_set_error('opponentscore',t('Bij een best-of-five win je nooit meer dan 3 games.'));
  if ($form_values['opponentscore'] == $form_values['contenderscore'])
    {
    form_set_error('contenderscore',t('Weet je zeker dat je een best-of-five hebt gespeeld?'));
    form_set_error('opponentscore',t('Een best-of-five eindigt nooit in gelijkspel.'));
    }
  if ((($form_values['opponentscore'] + $form_values['contenderscore']) > 5)|| (($form_values['opponentscore'] + $form_values['contenderscore']) < 3))
    {
    form_set_error('contenderscore', t('Weet je zeker dat je een best-of-five hebt gespeeld?'));
    form_set_error('opponentscore', t('Bij een best-of-five speel je in totaal 3, 4 of 5 games.'));
    }
  }


function poolmatch_matchform_new_submit($form, $form_values)
  {
  global $contender, $opponent, $poolid, $user;
  // TODO: transaction
  $mid = db_next_id('{poolmatch_matches}_mid');
  // create a match
  db_query('INSERT INTO {poolmatch_matches} (mid, pid, creator, ok) VALUES (%d, %d, %d, 0)', $mid, $poolid, $user->uid);
  // add contenderscore
  db_query('INSERT INTO {poolmatch_match_results} (mid, uid, score) VALUES (%d, %d, %d)', $mid, $contender, $form_values['contenderscore']);
  // add opponentscore
  db_query('INSERT INTO {poolmatch_match_results} (mid, uid, score) VALUES (%d, %d, %d)', $mid, $opponent, $form_values['opponentscore']);
  // send notification email
  $to = '';
  $subject = '';
  $adversary = '';
  switch ($user->uid)
    {
    case $contender:
      $to = user_load(array('uid'=>$opponent))->mail;
      $adversary = user_load(array('uid'=>$contender))->name;
      if ($form_values['opponentscore'] < $form_values['contenderscore']) $subject = t('Pwnt by ').user_load(array('uid'=>$contender))->name;
      else $subject = user_load(array('uid'=>$opponent))->name.t(' FTW!');
      break;
    case $opponent:
      $to = user_load(array('uid'=>$contender))->mail;
      $adversary = user_load(array('uid'=>$opponent))->name;
      if ($form_values['contenderscore'] < $form_values['opponentscore']) $subject = t('Pwned by ').user_load(array('uid'=>$opponent))->name;
      else $subject = user_load(array('uid'=>$contender))->name.t(' FTW!');
      break;
    }
    $body='Maar de door '.$adversary.' ingevoerde uitslag moet nog wel even door jou bevestigd dan wel ontkracht worden te '.url(substr(request_uri(),1), NULL, NULL, TRUE).'

    Liefs, de website.';
    drupal_mail('poolmatchmail', $to, $subject, $body);
    return 'poolmatch/pool/view/'.$poolid;
  }

/**
 * Admin settings
 */
function poolmatch_admin_settings()
  {
  $form['poolmatch_matchresult_nagging_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Herinneringsmail voor in te vullen wedstrijduitslagen'),
    '#description' => t('Vul het aantal uren in dat verstrijkt tussen het versturen van herinnerings-emails. Laat het veld leeg om nooit herinnerings-emails te versturen.'),
    '#default_value' => variable_get('poolmatch_matchresult_nagging_interval', ''),
    '#size' => 3,
    );

  $form['#validate'] = array(
    'poolmatch_admin_settings_validate' => array()
   );
   return system_settings_form($form);
  }

function poolmatch_admin_settings_validate($form_id, $form_values)
  {
  if (($form_values['poolmatch_matchresult_nagging_interval']) && (!is_numeric($form_values['poolmatch_matchresult_nagging_interval'])))
    {
    form_set_error('poolmatch_matchresult_nagging_interval', t('Vul een cijfer in.'));
    }
  }



function viewCompetition($cid = NULL)
  {
   if (!isset($cid))
    {
    $rows = Array();
    $sql = 'SELECT cid, descr, active FROM {poolmatch_competitions} ORDER BY active DESC,cid DESC';
    $result = db_query($sql);
    while ($data = db_fetch_object($result))
      {
      $rows[] = Array('<a href="/poolmatch/competition/view/'.$data->cid.'">'.$data->descr.'</a>',translateActive($data->active));
      }
    $title = t('Competitieoverzicht');
    return '<h2>'.$title.'</h2>'.theme_table(null, $rows);
    }

  else
    {
      //decorate
      $motherlinks = '';
      $daughterlinks = '';
      $result = db_query('SELECT pc2.cid, pc2.descr FROM {poolmatch_competitions} pc1, {poolmatch_competitions} pc2 WHERE pc2.cid = pc1.parent and pc1.cid = %d', $cid);
      while ($data = db_fetch_object($result))
	{
	$motherlinks.=l('<< '.$data->descr, 'poolmatch/competition/view/'.$data->cid);
	}
      $result = db_query('SELECT cid,descr FROM {poolmatch_competitions} WHERE parent = %d', $cid);
      while ($data = db_fetch_object($result))
	{
	$daughterlinks.=l($data->descr.' >>', 'poolmatch/competition/view/'.$data->cid).'<br/>';
	}
      drupal_add_css(POOLMATCH_PATH .'/poolmatch.css');
      $title = db_result(db_query('SELECT descr FROM {poolmatch_competitions} WHERE cid = %d', $cid));
      $html='<h2>'.$title.'</h2><div id="bla"> <div id="motherlinks">'.$motherlinks.'</div><div id="daughterlinks">'.$daughterlinks.'</div> </div><br><br>';

      //which pools are in this competition?
      $sql = 'SELECT pid FROM {poolmatch_pools} WHERE cid = %d ORDER BY rank';
      $result = db_query($sql, $cid);
      while ($data = db_fetch_object($result))
	{
	$overview .= viewPool($data->pid);
	}
      return $html.$overview;
    }
  }


function viewPool($pid = NULL)
  {
  if (!isset($pid))
    {
    return t('Geen pool-ID opgegeven...');
    }
  else
    {
    $sql = 'SELECT pp.rank, pu.uid FROM {poolmatch_pools} AS pp, {poolmatch_users} AS pu LEFT JOIN
	    (SELECT poolmatch_match_results.uid, SUM(poolmatch_match_results.score) 
	    AS total FROM {poolmatch_match_results}, {poolmatch_pools}, {poolmatch_matches} WHERE poolmatch_matches.pid = poolmatch_pools.pid AND poolmatch_match_results.mid = poolmatch_matches.mid AND poolmatch_matches.ok = 1 AND poolmatch_pools.pid = %d GROUP BY poolmatch_match_results.uid) as totals ON pu.uid = totals.uid
	    WHERE pp.pid = %d AND pu.pid = pp.pid ORDER BY totals.total DESC';
    //welke rank?
    $poolname = db_result(db_query($sql, $pid, $pid));
    //welke users?
    $result = db_query($sql, $pid, $pid);
    $allusers = Array();
    $poolmatrix = Array();

    while ($data = db_fetch_object($result))
      {
      $poolmatrix[$data->uid] = Array();
      $allusers[] = $data->uid;
      }

    foreach ($poolmatrix as $pooluser=>&$opponents)
      {
      $opponents = $allusers;
      }

    $header = Array(Array('data'=>'Bruin vs Zwart', 'bgcolor'=>'red'));
    $rows = Array();

    foreach ($poolmatrix as $contender=>$opponents)
      {
      $row = Array();
      $row[] =  Array('data'=>theme_username(user_load(Array(uid=>$contender))), 'bgcolor'=>'sienna');
      foreach ($opponents as $opponent)
	{
	$row[] = getScorePrettified($contender, $opponent, $pid);
	}

      $rows[] = $row;
      $userke = user_load(Array(uid=>$contender));
      $header[] = Array('data'=>poolmatch_theme_username($userke), 'bgcolor'=>'black');
      $emails[] = $userke->mail;
      }
      $maillist= '';
      while ($emails[1]){
        $maillist.= array_pop(&$emails).', ';
      }
      $maillist.= array_pop(&$emails);
      return theme_table($header, $rows, Array('cellspacing'=>'10', 'style'=>'font-size: 0.8em'), '<span style="font-size: large;">'.l('Pool '.$poolname, 'poolmatch/pool/view/'.$pid).'</span> <a href="mailto:'.$maillist.'"><img src="/misc/forum-new.png" title="Stuur een e-mail naar deze pool" alt="Stuur een email naar deze pool"/></a>');
    }
  }


function getScorePrettified($contender, $opponent, $pid)
  {
  global $user;
  $score = getScore($contender, $opponent, $pid);
  // playing matches against oneself would be silly
  if ($score == 'self') return Array('data'=>'', 'bgcolor'=>'dimgrey');
  // users' own results, or user is admin -> provide links to appropriate forms
  $is_editable = (($user->uid == $contender) || user_access('administer poolmatches'));
  if ($is_editable)
    {
    // empty result, link to result form
    if (count($score) == 0) return Array('data'=>'<a href="/poolmatch/matchresult/add/'.$contender.'/'.$opponent.'/'.$pid.'">'.t('toevoegen').'</a>', 'bgcolor'=>'DarkOliveGreen', 'align'=>'center');
    // result not OK'd yet, link to result form
    if ($score['ok'] == 0) return Array('data'=>'<a href="/poolmatch/matchresult/add/'.$contender.'/'.$opponent.'/'.$pid.'">'.t('keuren').'</a>', 'bgcolor'=>'DarkOrange', 'align'=>'center');
    }
  // regular, OK'd result
  if ($score['ok'] == 1) return Array('data'=>$score['score_user']." - ".$score['score_opponent'], 'align'=>'center');
  }


function getScore($contender, $opponent, $pid)
  {
  if ($contender == $opponent) return 'self';
  else
    {
    $sql = 'SELECT pmr1.score AS score_user, pmr2.score AS score_opponent, pm.ok
	    FROM {poolmatch_match_results} as pmr1, {poolmatch_match_results} as pmr2, {poolmatch_pools} AS pp, {poolmatch_matches} AS pm
	    WHERE pm.pid = pp.pid AND pmr1.mid = pm.mid AND pmr1.mid = pmr2.mid
	    AND pmr1.uid = %d AND pmr2.uid = %d AND pp.pid = %d';
    $score = db_fetch_array(db_query($sql, $contender, $opponent, $pid));
    return $score;
    }
  }

function translateActive($boolean){
  if ($boolean == 1) return t('Actief');
  return t('Gesloten');
}
