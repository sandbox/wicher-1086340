SET AUTOCOMMIT=0;
START TRANSACTION;

DROP TABLE IF EXISTS `poolmatch_competitions`;
CREATE TABLE "poolmatch_competitions" (
  "cid" int(11) NOT NULL default '0',
  "parent" int(11) default NULL,
  "descr" varchar(255) NOT NULL default '',
  "active" tinyint(1) NOT NULL default '0',
  PRIMARY KEY  ("cid"),
  KEY "active" ("active")
);

DROP TABLE IF EXISTS `poolmatch_matches`;
CREATE TABLE "poolmatch_matches" (
  "mid" int(11) NOT NULL auto_increment,
  "pid" int(11) NOT NULL default '0',
  "creator" int(11) NOT NULL,
  "created" timestamp NOT NULL default CURRENT_TIMESTAMP,
  "ok" tinyint(1) NOT NULL default '0',
  PRIMARY KEY  ("mid"),
  KEY "cid" ("pid","ok"),
  KEY "pid" ("pid")
);

DROP TABLE IF EXISTS `poolmatch_match_results`;
CREATE TABLE "poolmatch_match_results" (
  "rid" int(11) NOT NULL auto_increment,
  "mid" int(11) NOT NULL default '0',
  "uid" int(11) NOT NULL default '0',
  "score" int(11) NOT NULL default '0',
  PRIMARY KEY  ("rid"),
  KEY "mid" ("mid"),
  KEY "uid" ("uid")
);

DROP TABLE IF EXISTS `poolmatch_pools`;
CREATE TABLE "poolmatch_pools" (
  "pid" int(11) NOT NULL default '0',
  "cid" int(11) NOT NULL default '0',
  "rank" char(1) character set ascii NOT NULL default '',
  PRIMARY KEY  ("pid"),
  KEY "cid" ("cid")
);

DROP TABLE IF EXISTS `poolmatch_users`;
CREATE TABLE "poolmatch_users" (
  "id" int(11) NOT NULL auto_increment,
  "pid" int(11) NOT NULL default '0',
  "uid" int(11) NOT NULL default '0',
  PRIMARY KEY  ("id")
);
COMMIT;
