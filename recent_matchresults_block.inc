<?php

/**
 * @file
 * Work in progress, builds a block showing last X match results.
 *
 * @copyright Copyright (c) 2008-2011 Wicher Minnaard.
 *
 * Licensed under the terms of the GNU General Public License.
 * See http://drupal.org/licensing/faq#q1
 */


$content='';

$sql="SELECT u1.uid AS u1uid,u1.name AS u1name,u2.uid AS u2uid,u2.name AS u2name,pmr1.score AS u1score,pmr2.score AS u2score, pm.pid AS poolid FROM poolmatch_matches pm,poolmatch_match_results pmr1,poolmatch_match_results pmr2,users u1, users u2 WHERE pm.ok=1 AND pmr1.mid = pm.mid AND pmr2.mid = pm.mid AND pmr1.rid <> pmr2.rid AND pmr1.uid = u1.uid AND pmr2.uid = u2.uid AND pmr1.uid < pmr2.uid AND pm.created > (CURDATE() - INTERVAL 21 DAY) ORDER BY pm.created DESC LIMIT 5";

function prettyuser($uid, $name){
  if (drupal_strlen($name) > 8) {
    $name = trim(drupal_substr($name, 0, 7)).'…';
  }
  $pretty = '<a href="/user/'.$uid.'">'.$name.'</a>';
  return $pretty;
}

function prettyscore($sc1, $sc2, $pid){
  $pretty = '<a href="/poolmatch/pool/view/'.$pid.'">'.$sc1.'—'.$sc2.'</a>';
  return $pretty;
}

$trows=array();
$result = db_query($sql);
while ($match = db_fetch_array($result)) {
  $trow=array();
  $trow[]= prettyuser($match[u1uid],$match[u1name]);
  $trow[]= '—';
  $trow[]= prettyuser($match[u2uid],$match[u2name]);
  $trow[]= prettyscore($match[u1score],$match[u2score], $match[poolid]);
  $trows[]=$trow;
}

$attributes[style]= 'font-size: 9px; line-height: 1em;';

return theme_table(NULL, $trows, $attributes);

?>
